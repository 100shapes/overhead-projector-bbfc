const palette = {
  black: 'black',
  coldGrey: '#829FAA',
  grey: '#5E5E5E',
  white: 'white',
};

const colorModes = {
  default: {
    background: palette.white,
    muted: palette.grey,
    text: palette.black,

    // Special components
    heading: palette.black,
    logomark: palette.black,
  },
  options: {
    coldGrey: {
      background: palette.coldGrey,
      muted: palette.grey,
      text: palette.white,

      // Special components
      heading: palette.white,
      logomark: palette.white,
    },
    grey: {
      background: palette.grey,
      muted: palette.coldGrey,
      text: palette.white,

      // Special components
      heading: palette.white,
      logomark: palette.white,
    },
    black: {
      background: palette.black,
      muted: palette.grey,
      text: palette.white,

      // Special components
      heading: palette.white,
      logomark: palette.white,
    },
  },
};

const theme = {
  fonts: {
    body: 'ProximaNova, sans-serif',
    heading: 'ProximaNova, sans-serif',
    display: '"Yorkten Slab", Georgia, serif',
    monospace: 'Corrier, monospace',
  },
  fontWeights: {
    body: 400,
    heading: 600,
  },
  colors: {
    ...palette,
    ...colorModes.default,
    modes: colorModes.options,
  },
  textStyles: {
    display: {
      fontFamily: 'display',
      fontSize: '6.25vw',
      mb: 0,
      zIndex: 1,
    },
    small: {
      fontSize: 2,
    },
  },
  styles: {
    h1: {
      color: 'heading',
      m: 0,
    },
    h2: {
      mb: 0,
    },
  },
};

export default theme;

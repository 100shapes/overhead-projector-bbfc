import React from 'react';
import { jsx, useThemeUI } from 'theme-ui';

const useMutedColor = (bg) => {
  const context = useThemeUI();
  const {
    theme: { colors },
    colorMode,
  } = context;

  const fill = bg
    ? bg
    : colorMode === 'default'
    ? colors.muted
    : colors.modes[colorMode].muted;

  return fill;
};

export default useMutedColor;

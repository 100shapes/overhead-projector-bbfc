/** @jsx jsx */
import { jsx } from 'theme-ui';
import React from 'react';

const Columns = ({ children, columnCount = 2 }) => {
  return (
    <div
      sx={{
        columnCount: columnCount,
        columnGap: 5,
      }}
    >
      {children}
    </div>
  );
};

export default Columns;

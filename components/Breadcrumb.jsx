/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';
import Header from '@mdx-deck/gatsby-plugin/src/header';
import intersperse from '../helpers/intersperse';

const Breadcrumb = ({ trail: parts }) => {
  const trail = intersperse(parts, ' • ');

  return (
    <Header
      sx={{
        p: 0,
      }}
    >
      <div
        sx={{
          mx: 5,
          my: 3,
        }}
      >
        <Styled.p
          sx={{
            py: 2,
            px: 3,
            fontSize: 4,
            bg: 'text',
            m: 0,
            color: 'background',
          }}
        >
          {trail}
        </Styled.p>
      </div>
    </Header>
  );
};

export default Breadcrumb;

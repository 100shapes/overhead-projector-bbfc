/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';
import Footer from '@mdx-deck/gatsby-plugin/src/footer';
import intersperse from '../helpers/intersperse';
import { Logo } from '../components';

const ShapesFooter = ({ numPages, currentIndex, withLogo = true }) => {
  const currentPageDisplay = currentIndex + 1;
  const pageNumbers = intersperse([currentPageDisplay, numPages], ' / ');
  return (
    <Footer
      sx={{
        mx: 5,
        px: 0,
        py: 3,
        borderTop: '2px solid currentColor',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      {withLogo && <Logo />}
      <Styled.p
        sx={{
          variant: 'textStyles.small',
          m: 0,
        }}
      >
        Copyright &copy; {new Date().getFullYear()} 100 Shapes Ltd. All rights
        reserved.
      </Styled.p>

      <Styled.p
        sx={{
          variant: 'textStyles.small',
          m: 0,
        }}
      >
        {pageNumbers}
      </Styled.p>
    </Footer>
  );
};

export default ShapesFooter;

/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';
import Base from './Base';

const TitleLayout = ({ heading, ...props }) => {
  return (
    <Base {...props}>
      <Styled.p
        sx={{
          variant: 'textStyles.display',
          display: 'block',
        }}
      >
        bbfc Horizon
      </Styled.p>
      <Styled.h1
        sx={{
          variant: 'textStyles.display',
          fontSize: '5vw',
        }}
      >
        {heading}
      </Styled.h1>
    </Base>
  );
};

TitleLayout.defaultProps = {
  colorVariant: 'black',
};

export default TitleLayout;

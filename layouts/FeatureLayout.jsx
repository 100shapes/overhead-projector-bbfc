/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import { Image } from 'mdx-deck';
import React from 'react';
import Base from './Base';

const FeatureLayout = ({ children, featureSrc, ...props }) => {
  return (
    <Base {...props}>
      <div
        sx={{
          display: 'grid',
          gridTemplateColumns: ['repeat(1, 1fr)', null, 'repeat(2, 1fr)'],
          gridGap: 5,
          alignItems: 'center',
        }}
      >
        <div>{children}</div>
        <div
          sx={{
            height: '70vh',
          }}
        >
          <Image src={featureSrc} />
        </div>
      </div>
    </Base>
  );
};

FeatureLayout.defaultProps = {
  colorVariant: 'default',
};

export default FeatureLayout;

/** @jsx jsx */
import { jsx, useColorMode } from 'theme-ui';
import React from 'react';
import { useDeck } from 'mdx-deck';
import { GlobalStyles, Breadcrumb, ShapesFooter } from '../components';

const Base = ({
  breadcrumb = [],
  colorVariant = 'default',
  withLogo = true,
  children,
}) => {
  const [mode, setMode] = useColorMode();
  const deck = useDeck();

  React.useEffect(() => {
    if (mode !== colorVariant) {
      setMode(colorVariant);
    }
  });

  return (
    <>
      <GlobalStyles />
      {breadcrumb.length > 0 && <Breadcrumb trail={breadcrumb} />}

      <div
        sx={{
          px: 5,
          py: 6,
          alignSelf: 'flex-start',
          height: '100%',
          width: '100%',
        }}
      >
        {children}
      </div>

      <ShapesFooter numPages={deck.length} currentIndex={deck.index} />
    </>
  );
};

export default Base;

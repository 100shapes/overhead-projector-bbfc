/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';
import Base from './Base';

const ContentLayout = ({ children, ...props }) => {
  return <Base {...props}>{children}</Base>;
};

ContentLayout.defaultProps = {
  colorVariant: 'default',
};

export default ContentLayout;
